// console.log("Hello!");

/*
	You can directly pass data into the function. The function can then call/use that data which is referred as "name"
*/

/*
	Syntax:
	function functionName(parameter) {
		code block
	}
	functionName(argument)
*/

/*
	"parameter" acts as a named variable/container that exists only inside of a function

	it is used to store information that is provided to a function when it is called/invoked
*/

// name is called parameter
function printName(name) {
	console.log("My name is "+ name);
}

// "Juana" and "Anne", the information/data provided directly into the function, it is called an argument
printName("Juana");
printName("Anne");

let userName = "Annej";

printName(userName);
printName(name);

function greeting() {
	console.log("Hello, User!");
}

greeting("Eric");

// Using Multiple Parameters
function creatingFullName (firstName, middleName, lastName){
	console.log(firstName +" "+ middleName + " "+ lastName);
}

creatingFullName("Annejanette", "Orca", "Real");

// In JS, providing more/less arguments than the expected parameters will not return an error
creatingFullName("Annejanette", "Real");
creatingFullName("Annejanette", "Orca", "Real", "Ortiz");

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of "+ num +" divided by 8 is "+remainder);
	let isDivisibleBy8 = (remainder === 0);
	console.log("Is "+ num +" divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	Mini-activity:
		1. Create a function which is able to receive data as an argument.
			- This function should be able to receive the name of your favorite superhero
			- Display the name of your favorite superhero in the console.

	Kindly the ss in ouy gc.
*/

function displayFavHero(superHero){
	console.log("My favorite superhero: "+ superHero);
}

displayFavHero("Iron Man");

// Return statement
/*
	The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function
*/

function returnFullName(firstName, middleName, lastName) {
	return firstName +" "+ middleName +" "+ lastName; //end statement
	console.log("Can we print this message?");
}

// whatever value that is returned from the "returnFullName" function can be stored in a variable
let completeName = returnFullName("Anne", "Orca", "Real");
console.log(completeName);
console.log(returnFullName("Anne", "Orca", "Real"));

function returnAddress(city, country) {
	let fullAddress = city +", "+ country;
	return fullAddress
}

let myAddress = returnAddress("Sta. Mesa, Manila", "Philippines");
console.log(myAddress);

// consider the ff code

/*
	Mini-Activity:
		1. Debug our code. So that the function will be able to return a value and save it in a variable
*/

function printPlayerInformation(userName, level, job){
	console.log("Username: "+ userName);
	console.log("Level: "+level);
	console.log("Job: "+ job);
}

let user1 = printPlayerInformation("cardo123", "999", "Immortal");
console.log(user1);

function printPlayerInformation1(userName, level, job){
	return "Username: "+ userName+ "\nLevel: "+level +"\nJob: "+ job;
}

let user2 = printPlayerInformation1("cardo123", "999", "Immortal");
console.log(user2);

/*
	- returns undefined because printPlayerInformation returns nothing. It only logs the message in the console.
	- You cannot save any value from printPlayerInfo() because it does not RETURN anything
*/