// console.log("Hello"); test connection

function addTwoNumbers(num1, num2) {
	let sum = num1 + num2;
	console.log("Displayed sum of "+ num1 +" and "+ num2);
	console.log(sum);
}

addTwoNumbers(5, 15);

function subtractTwoNumbers(num1, num2) {
	let difference = num1 - num2;
	console.log("Displayed difference of "+ num1 +" and "+ num2);
	console.log(difference);
}

subtractTwoNumbers(20, 5);


function multiplyTwoNumbers(num1, num2) {
	let productNum = num1 * num2;
	console.log("The product of "+ num1 +" and "+ num2 +":");
	return productNum;
}

let product = multiplyTwoNumbers(50, 10);
console.log(product);

function divideTwoNumbers(num1, num2) {
	let quotientNum = num1 / num2;
	console.log("The quotient of "+ num1 +" and "+ num2 +":");
	return quotientNum;
}

let quotient = divideTwoNumbers(50, 10);
console.log(quotient);

function getCircleArea(radius) {
	let circleArea = 3.14 * (radius**2);
	console.log("The result of getting the area of a circle with "+ radius +" radius:");
	return circleArea;
}

let circleArea = getCircleArea(15);
console.log(circleArea);

function getAverage(firstGrade, secondGrade, thirdGrade, fourthGrade) {
	let average = (firstGrade + secondGrade + thirdGrade + fourthGrade) / 4;
	console.log("The average of "+ firstGrade +", "+ secondGrade +", "+ thirdGrade +", and "+ fourthGrade +":");
	return average;
}

let averageVar = getAverage(20, 40, 60, 80);
console.log(averageVar);

function checkPassed(score, totalScore){
	let percentage = (score / totalScore) * 100;
	let isPassed = 75 <= percentage;
	console.log("Is "+ score +"/"+ totalScore+ " a passing score?");
	return isPassed;
}

let isPassingScore = checkPassed(38, 50);
console.log(isPassingScore);